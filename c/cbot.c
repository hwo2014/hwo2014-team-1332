#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include "cJSON.h"

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
//static cJSON *create_race(char *bot_name, char *bot_key);
//static cJSON *join_race(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *switch_msg(char *direction);
static cJSON *make_msg(char *type, cJSON *msg);
static double mod(double x);
static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static int isTurn(cJSON *piece);
static double getRadius(cJSON *piece);
static double getAngle(cJSON *piece);
static double getLength(cJSON *piece);
static cJSON *turbo_msg();
static double getMinDistance(cJSON *turn,double acceleration, double velocity,double friction);
//static double getMinAngleDist(cJSON *turn,double acceleration, double velocity,double,double);
//static double getMinAngle(cJSON *turn,double acceleration, double velocity);
//static double getTurnVelocitySquare(cJSON *turn);
static double getTurnVelocity(cJSON *turn);
static double getThrottle(cJSON *piece);
static int checkSwitch(cJSON *piece);
static double current_carAngle = 0,previous_carAngle = 0;
static double Vmax ,coeff_friction=0,current_lanePosition=0;
static int turboFired=0;
static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
    int sock=0,laps=0,current_lap=0;
    cJSON *json,*data_gamePath,*lanes;
    double current_throttle;
    double current_velocity=0,previous_velocity=0;
    double current_angularVelocity = 0,previous_angularVelocity = 0;
    double current_position = 0,previous_position = 0;
    double distance,min_throttle_required = 1.0,			previous_guess=0,current_guess=0;
  //  int min_throttle_turn=0;
    double acceleration = 0,max_acceleration=0,angular_acceleration = 0;
    double completed_piece_length=0,time_step = (double)1/(double)60;
    int current_pieceIndex = 0,previous_pieceIndex = 0,count_next_turns = 0,pieceIndexIterator = 0;
	
    int switched=0,nextTurn=0,current_lane=0,fire_point=0;
	int VmaxSet = 0;
//	double length=0,width=0;
	//double previous_guess=0,current_guess=0;
	int size=0,counter=0;
	//double turbo_throttle=-1;
	int accelerationSet = 0,turboActivated = 0,final_lap=0;
	int current_tickCount = 0,turboTicks = 0,turboPathFinished=0;
    if (argc != 5)
        error("Usage: bot host port botname botkey\n");
   //     error(" botname botkey: %s\t%s\n",argv[3], argv[4]);
    sock = connect_to(argv[1], argv[2]);
	
    json = join_msg(argv[3], argv[4]);
  //  json = create_race(argv[3], argv[4]);
  	//printf("ishal");
  //  json = join_race(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);

    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type, *data_position,*piecePosition,*inPieceDistance,*myCar;
        char *msg_type_name;
        double pieceDistanceValue;
	      char *tick,*track,*pieceDistance,*pieceIndexChar;
	      cJSON *gameTick,*pieceIndex;
	      int pieceIndexValue;

	      
        msg_type = cJSON_GetObjectItem(json, "msgType");
//	printf("%s",cJSON_Print(msg_type));
	if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
	if(!strcmp("createRace", msg_type_name)) {
//		printf("done");
	//msg = join_race("Striker","k8qSjzRU5MHDeA");
	}
	
	
	 if(!strcmp("gameInit", msg_type_name)) {
		
		data_gamePath = cJSON_Duplicate(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(json, "data"),"race"),"track"),"pieces"),1);
		lanes = cJSON_Duplicate(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(json, "data"),"race"),"track"),"lanes"),1);
	//	cars = cJSON_Duplicate(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(json, "data"),"race"),"cars"),1);
//		
//		length = atof(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetArrayItem(cars,0),"dimensions"),"length")));
//		width = atof(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetArrayItem(cars,0),"dimensions"),"width")));
		//guideFlagPosition = atof(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetArrayItem(cars,0),"dimensions"),"guideFlagPosition")));
//		*data_gamePath = *temp;
		
		laps = atoi(cJSON_Print(cJSON_GetObjectItem(cJSON_Duplicate(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(json, "data"),"race"),"raceSession"),1),"laps")));
	//	printf("\n\n\n\n\nLaps:%d\n\n\n\n\n",laps);
		size = cJSON_GetArraySize(data_gamePath);
		track = cJSON_Print(json);
//		puts(track);
		//d=4;
		msg = ping_msg();
	//	if(cJSON_GetObjectItem(cJSON_GetArrayItem(data_gamePath,d),"angle")!=NULL)
	//	printf("\n\n Size : %d\t%d\n",size,atoi(cJSON_Print(cJSON_GetObjectItem(cJSON_GetArrayItem(data_gamePath,d),"angle"))));
	//	printf("\n\n Dimensions : %.6g\t%.6g\t%.6g\n",length,width,guideFlagPosition);


	}
	if(!strcmp("gameStart", msg_type_name))
	{
	
//		printf("\n%s\n",cJSON_Print(json));
//		msg = throttle_msg(1);
msg = ping_msg();	
	}
	 if(!strcmp("turboAvailable", msg_type_name))
	{
		turboActivated = 1;
		//printf("\nTurbo Activated : %s\n",cJSON_Print(json));
		turboTicks = atoi(cJSON_Print(cJSON_Duplicate(cJSON_GetObjectItem(cJSON_GetObjectItem(json,"data"),"turboDurationTicks"),1)));
		
		//error("Turbo Activated");
	}
	
	 if (!strcmp("carPositions", msg_type_name)) {
//		printf("vishal");
		gameTick = cJSON_GetObjectItem(json, "gameTick");
		//printf("\n\n Size :%d\n\n\n",size);
		if(gameTick !=NULL)
		{		
			tick = cJSON_Print(gameTick);
		//	printf("tick:%s\n",tick);
		}

		data_position = cJSON_GetObjectItem(json, "data");
		
		
		
		counter = 0;
//		printf("\nIndex: %d\n",strcmp(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetArrayItem(data_position,counter),"id"),"name")),"\"Striker\""));


		while(strcmp(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetArrayItem(data_position,counter),"id"),"name")),"\"Striker\"") != 0)
		{
//			printf("vishal");
			counter++;
			
		}

		myCar = cJSON_GetArrayItem(data_position,counter);
	
	
	//	printf("here1\n");
		current_lane = atoi(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(myCar, "piecePosition"),"lane"),"endLaneIndex")));
		current_lanePosition = atof(cJSON_Print(cJSON_GetObjectItem(cJSON_GetArrayItem(lanes,current_lane),"distanceFromCenter")));
	//	start_lane = atoi(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(cJSON_GetObjectItem(myCar, "piecePosition"),"lane"),"startLaneIndex")));
		current_lap = atoi(cJSON_Print(cJSON_GetObjectItem(cJSON_GetObjectItem(myCar, "piecePosition"),"lap")));
		//	printf("\n%d\n",current_lap);
		if(checkSwitch(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)) == 1)
			switched = 0;

		current_carAngle = atof(cJSON_Print(cJSON_GetObjectItem(myCar,"angle")));
//		//printf("datasize:%d\n",cJSON_GetArraySize(data));
	//			printf("here2\n");
		piecePosition = cJSON_GetObjectItem(myCar, "piecePosition");
		inPieceDistance = cJSON_GetObjectItem(piecePosition, "inPieceDistance");
		pieceIndex = cJSON_GetObjectItem(piecePosition, "pieceIndex");
		pieceIndexChar = cJSON_Print(pieceIndex);
		pieceIndexValue = atoi(pieceIndexChar);
		current_pieceIndex = pieceIndexValue;
		
		
		if(inPieceDistance != NULL)
		{		
			pieceDistance = cJSON_Print(inPieceDistance);
			pieceDistanceValue = atof(pieceDistance);
		//	printf("distance:%.6g\n",(double)pieceIndexValue*(double)100+pieceDistanceValue);
		//	printf("distance:%d\n",pieceIndexValue);
		//	puts(pieceDistance);
		//	puts("\n");		
		}	  
	    
	  //  printf("Current piece Index:%d\n",current_pieceIndex);
	//    pr	intf("Is turn:%d\n\n",isTurn(cJSON_GetArrayItem(data_gamePath,4)));
		            
	    if(current_pieceIndex != previous_pieceIndex)
	    {

	     // printf("Indexes current and previous: %d\t%d",current_pieceIndex,previous_pieceIndex);
	        completed_piece_length += getLength(cJSON_GetArrayItem(data_gamePath,previous_pieceIndex));	
		
	}

       // printf("Completed Piece Length: %.6g\n",completed_piece_length);
	    current_position =  completed_piece_length + pieceDistanceValue;	
		current_velocity = (current_position-previous_position)/time_step;
		current_angularVelocity = mod(current_carAngle-previous_carAngle)/time_step;
		
		acceleration = mod(current_velocity-previous_velocity)/time_step;	
		angular_acceleration =  mod(current_angularVelocity-previous_angularVelocity)/time_step;	
		if(current_velocity > previous_velocity && accelerationSet == 0)
		{

			if(acceleration > max_acceleration && atoi(tick)<6 && atoi(tick)>1)
			{
				max_acceleration = acceleration;
				coeff_friction = acceleration;		//coeff_friction is actually mu*g
				if(atoi(tick) ==3)
					accelerationSet = 1;
			}
			if((current_velocity/current_throttle)>Vmax && atoi(tick)<3)
			{
					Vmax = (current_velocity/current_throttle);
			}
		}
		
		//	Vmax = 1E+15;
// Recursion for solving for Vmax
			previous_guess = 1000;
			
		if(gameTick!=NULL && atoi(tick)<10 && atoi(tick)>4 && VmaxSet==0 && current_velocity>0 && previous_velocity>0)
		{
		
			while(1)
			{
				current_guess = current_velocity/(1-pow(1-(previous_velocity/previous_guess),(double)(atoi(tick)-1)/(double)(atoi(tick)-2)));	
				//printf("\n guess: %.6g\t%.6g",previous_guess,current_guess);				
				if(mod(current_guess-previous_guess)/previous_guess < 1E-5)
					break;		
				else previous_guess = current_guess;	
			}
			Vmax = current_guess;
			VmaxSet = 1;
			//Vmax = 600;
		//	Vmax = 1E+15;
			//printf("\n\n Vmax: %.6g",Vmax);
			//if(atoi(tick)==5)
			//VmaxSet = 1;
		}
		else if(VmaxSet==0)
			Vmax = 1E+15;
		//	printf("From inside :%.6g\t%.6g\t%.6g\t%.6g\t%s\n",current_position,acceleration,current_velocity,previous_velocity,Vmax);
/*		
		if(gameTick!=NULL)
		{
			if(atoi(tick) == 3)
			{
				exponent = log(((current_velocity-previous_velocity)/(previous_velocity)));
				A = (previous_velocity)*exponent/(exp(-exponent*time_step)-1);
//				Vmax = A/exponent;
				Vmax = (double)360/(double)0.6;
				//printf("exponent: %.6g\t%.6g\n\n",exponent,A);
			}
		//	printf("%d\t%.6g\t%.6g\t%.6g\t%.6g\t%.6g\t%.6g\t%.6g\t%d\t%.6g\n",atoi(tick),current_position,acceleration,current_velocity,Vmax,current_angularVelocity,angular_acceleration,current_carAngle,current_pieceIndex,current_throttle);
		}
*/		
        	//printf("%.6g\t%.6g\t%.6g\t%.6g\n",current_position,acceleration,current_velocity,Vmax);
//	printf("%d\t%.6g\t%.6g\t%.6g\t%.6g\t%.6g\t%.6g\t%.6g\t%d\t%.6g\n",atoi(tick),current_position,acceleration,current_velocity,Vmax,current_angularVelocity,angular_acceleration,current_carAngle,current_pieceIndex,current_throttle);
		

 		    count_next_turns = 0;
        	//While loop finds out upcoming 3 turns and tries to determine wether bot will be able to slow down 
        	// or not in remaining distance till it reaches maximum permeable speed at that turn
			min_throttle_required = 1.0;
			//printf("\nhereasfasdas\n\n");
			
			msg = throttle_msg(min_throttle_required);
				if(isTurn(cJSON_GetArrayItem(data_gamePath,current_pieceIndex))==1 )			

	//	printf("\n On turn min velocity:%.6g\t%.6g\n",getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)),	pieceDistanceValue);

	nextTurn =-1;

			
/*		if(current_pieceIndex == 28)
				msg = throttle_msg(0.45);
		else 	
*/	
	

counter=0;
	//	printf("\nTurboStatus: turboFired: %d\tTurboActivated:%d\t turboTicks:%d\tcurrentTickCount:%d\n",turboFired,turboActivated,turboTicks,current_tickCount);
		
		if(turboActivated == 1 && turboFired == 1 && gameTick !=NULL && current_pieceIndex == turboPathFinished)
				turboFired = 0;
				
		pieceIndexIterator = current_pieceIndex;
		while(isTurn(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator))==0 && turboActivated == 1)
		{
	//		printf("\nhere1 msg\n");
			if((pieceIndexIterator == size-1 || counter>=6) && turboFired == 0)
			{
	//		printf("\nhere3 msg\n");			
				if(current_lap == laps-1 || counter>=6)
				{
				msg = turbo_msg();
				turboFired = 1;
				fire_point=1;
				current_tickCount = atoi(tick);
				}
				
				if(current_lap == laps-1)
					final_lap=1;
				//break;
			
			}
			pieceIndexIterator+=1;
			counter++;
			pieceIndexIterator = pieceIndexIterator%size;
		
		}
		turboPathFinished = pieceIndexIterator;
		if(fire_point == 1)
		{
//			printf("\nhere3 msg\n");		
			fire_point =0;
//			msg = throttle_msg(1);
		}

		else if (final_lap == 1 && atoi(tick) >= (current_tickCount+turboTicks))
		{
	//		printf("\nhere4 msg\n");		
			msg = throttle_msg(1);
		}
		/*
		else if(turboFired == 1)
		{
			if(isTurn(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)) ==0)
			{
				pieceIndexIterator = current_pieceIndex;
				while(isTurn(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)) == 0)
				{
				
					pieceIndexIterator++;	
					pieceIndexIterator = pieceIndexIterator%size;					
				}
				if(current_velocity > getTurnVelocity(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)))
					msg = throttle_msg(0);
				else turboFired = 0;			
			}	
			else{
				if(current_velocity > getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))
					msg = throttle_msg(0);
				else turboFired = 0;			
			
				
			}
			//			msg = throttle_msg(0);
		
		}
		*/
		
		else if(isTurn(cJSON_GetArrayItem(data_gamePath,current_pieceIndex))==1 &&
		current_velocity > getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))
		{
 	      	distance = mod(pieceDistanceValue-getLength(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)));
        	pieceIndexIterator = current_pieceIndex+1;
		    pieceIndexIterator = pieceIndexIterator%size;
		
			while(isTurn(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)) != 0 )
			{
			
				distance += getLength(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator));
				pieceIndexIterator+=1;
				pieceIndexIterator = pieceIndexIterator%size;
				if(isTurn(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)) != 0 &&
				getRadius(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)) != getRadius(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))	
				{
				
					nextTurn = pieceIndexIterator;
					break;
				}
				
			}
			distance += getLength(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator));
	//		printf("\nValues: %.6g\t%.6g\t%.6g\t%.6g\t%.6g\n",current_angularVelocity,distance,getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)),coeff_friction,mod(current_angularVelocity*distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))));
		
		
			if(mod(current_angularVelocity*distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))+0.5*angular_acceleration*(distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex))))*distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))) < mod((double)50-mod(current_carAngle)) &&
			nextTurn != -1 &&
			 current_velocity < getTurnVelocity(cJSON_GetArrayItem(data_gamePath,nextTurn)))
			{
				msg = throttle_msg(1);
			}
			else if(mod(current_angularVelocity*distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))+0.5*angular_acceleration*(distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex))))*distance/(getTurnVelocity(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)))) < mod((double)50-mod(current_carAngle)) &&
			nextTurn == -1 ){
			
							msg = throttle_msg(1);
			
			}
			else
			{
	  //  		printf("vishal");
	          	min_throttle_required = getThrottle(cJSON_GetArrayItem(data_gamePath,current_pieceIndex));			
	   //			printf("\n On turn:%.6g\n",min_throttle_required);
				msg = throttle_msg(min_throttle_required);
			}
		}
else {

		//	printf("\nhere5 msg\n");
        	
        	distance = mod(pieceDistanceValue-getLength(cJSON_GetArrayItem(data_gamePath,current_pieceIndex)));
        	pieceIndexIterator = current_pieceIndex+1;
        			    pieceIndexIterator = pieceIndexIterator%size;
nextTurn = -1;
while(count_next_turns<2)
        	{
        		if(isTurn(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator))==1)
        		{
        			if(count_next_turns==0 && pieceIndexIterator != (int)((current_pieceIndex+1)%size))
        				nextTurn = pieceIndexIterator;
        			else if(count_next_turns==1 && nextTurn == -1)
        				nextTurn = pieceIndexIterator;
        			//printf("\n\nfound turn: %d\n\n",pieceIndexIterator);
        			count_next_turns += 1;       
        			//temp_friction = current_velocity*current_velocity/getRadius(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)); 

  /*      if(isTurn(cJSON_GetArrayItem(data_gamePath,current_pieceIndex))==1 && 
                mod(current_carAngle) < 85.0  &&
        mod(85-current_carAngle) > getMinAngle(cJSON_GetArrayItem(data_gamePath,current_pieceIndex),angular_acceleration,current_angularVelocity) &&
        distance > getMinAngleDist(cJSON_GetArrayItem(data_gamePath,current_pieceIndex),angular_acceleration,current_angularVelocity,acceleration,current_velocity)  &&
         distance > getMinDistance(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator),acceleration,current_velocity,temp_friction) &&
          coeff_friction < temp_friction)
		{
				coeff_friction = temp_friction;
				min_throttle_required = 1.0;
				break;
		
		}
        else if(isTurn(cJSON_GetArrayItem(data_gamePath,current_pieceIndex))==1 && 
        mod(current_carAngle) < 85.0  && 
         mod(85-current_carAngle) > getMinAngle(cJSON_GetArrayItem(data_gamePath,current_pieceIndex),angular_acceleration,current_angularVelocity) &&
        distance > getMinAngleDist(cJSON_GetArrayItem(data_gamePath,current_pieceIndex),angular_acceleration,current_angularVelocity,acceleration,current_velocity) &&
               distance > getMinDistance(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator),acceleration,current_velocity,coeff_friction))
        {
        
        
        	min_throttle_required = 1.0;
        	break;
        }
		else 
		
*/		
if(nextTurn!=-1 && checkSwitch(cJSON_GetArrayItem(data_gamePath,(int)((current_pieceIndex+1)%size)))==1  && getAngle(cJSON_GetArrayItem(data_gamePath,nextTurn)) > 0 && switched == 0){
		//			printf("\nAngle:%.6g",getAngle(cJSON_GetArrayItem(data_gamePath,nextTurn)));
		//			printf("\n\n\nSwitched  right\n\n\n\n");	        		
        				msg = switch_msg("Right");	
        				switched = 1;
        				break;		
        		}
        		else if(nextTurn!=-1 && checkSwitch(cJSON_GetArrayItem(data_gamePath,(int)((current_pieceIndex+1)%size)))==1  && getAngle(cJSON_GetArrayItem(data_gamePath,nextTurn)) < 0 && switched == 0)
        		{
 		//		printf("\n\n\nSwitched left\n\n\n\n");	        		
        			msg = switch_msg("Left");	
        			switched = 1; 
        			break;       			
        		}


else		if(current_velocity > getTurnVelocity(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)) && 
		((distance < getMinDistance(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator),acceleration*((double)4/(double)3),current_velocity,coeff_friction) && turboFired == 0) || (distance < getMinDistance(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator),acceleration*((double)1/(double)2),current_velocity,coeff_friction) && turboFired==1) ))
        			{
  //      				printf("\n\nViishal narkhede\n\n");
        				if(getThrottle(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator)) < min_throttle_required)
        					{
        					  min_throttle_required = getThrottle(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator));			
        						//min_throttle_turn = pieceIndexIterator;
        						msg = throttle_msg(min_throttle_required);
        					}
        				else; 
        			}
        		
	
		 
		 }       		
        			distance = distance+getLength(cJSON_GetArrayItem(data_gamePath,pieceIndexIterator));
        			pieceIndexIterator+=1;
        			pieceIndexIterator = pieceIndexIterator%size;
        	}

		}

		//msg = throttle_msg(0.6);
        	previous_pieceIndex = current_pieceIndex;
            previous_position = current_position;
        	previous_velocity = current_velocity;
			previous_carAngle = current_carAngle;
			previous_angularVelocity = current_angularVelocity;
			//previous_lane = current_lane;
			current_throttle = min_throttle_required;
        } else {
  //      printf("\nbeforethrottle:%s\n",msg_type_name);
            log_message(msg_type_name, json);
            msg = ping_msg();
        }
/*

	if(current_pieceIndex>5)
	msg = throttle_msg(0.0);
	else{	
       msg = throttle_msg(0.7);
	}
*/
   //  msg = throttle_msg(0.55);
	//printf("\nThrottle:%.6g",min_throttle_required);
    	if(gameTick==NULL)
		{
		ping_msg();
        }
//        		printf("\nInside if condition\n");
        
        write_msg(sock, msg);
        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

    return 0;
}

static int isTurn(cJSON *piece){

         		if(cJSON_GetObjectItem(piece,"angle")!=NULL)
                        return 1;
                        else return 0;
}

static double getRadius(cJSON *piece)
{

	cJSON *temp;
	double radius;
	char *temp_value;
	
	temp = cJSON_GetObjectItem(piece, "radius");
	temp_value = cJSON_Print(temp);
	radius = atof(temp_value);
	if(getAngle(piece) > 0)
		return radius+current_lanePosition;
	else return radius-current_lanePosition;               // here +10 is done because of lane position...dont forget to change that :)
}

static double getAngle(cJSON *piece){

	cJSON *temp;
	double angle;
	char *temp_value;
	
	temp = cJSON_GetObjectItem(piece, "angle");
	temp_value = cJSON_Print(temp);
	angle = atof(temp_value);
	
//	printf("\n\nAngle: %.6g\n\n",angle);
    return angle;
}

static double getLength(cJSON *piece)
{
	cJSON *temp1;
	double length,pie=(double)22/(double)7;
	char *temp_value1;
	if(isTurn(piece)==0)
	{
		temp1 = cJSON_GetObjectItem(piece, "length");
		temp_value1 = cJSON_Print(temp1);
		length = atof(temp_value1);
		//printf("LENGTH:%.6g\n\n\n",length);
	}
	else{
		length = mod(getAngle(piece))*getRadius(piece)*pie/(double)180;
	
	}
	
	return length;
	
	
}

static int checkSwitch(cJSON *piece){

	         		if(cJSON_GetObjectItem(piece,"switch")!=NULL)
                        return 1;
                        else return 0;


}
static double getMinDistance(cJSON *turn,double acceleration,double velocity,double friction){
 //  printf("\n\nYup here i m in Getmindistance\n\n");
		
	
	double mindist = -(friction*getRadius(turn)-(velocity*velocity))/(2*acceleration);
	//printf("\nMinimum distance required:%.6g\t%.6g\n",mindist,coeff_friction);
	return	mindist;

}
/*
static double getMinAngleDist(cJSON *turn,double Aacceleration,double Avelocity,double acceleration,double velocity){
 //  printf("\n\nYup here i m in Getmindistance\n\n");
		
	
	
	double minAngle = -(-(Avelocity*Avelocity))/(2*Aacceleration);
	double time = -Avelocity+sqrt((Avelocity*Avelocity)+(2*Aacceleration*minAngle))/Aacceleration;
	double dist = velocity*time-0.5*acceleration*time*time;
	//printf("\nMinimum distance required:%.6g\t%.6g\n",mindist,coeff_friction);
	return	dist;

}

static double getMinAngle(cJSON *turn,double Aacceleration,double Avelocity){
 //  printf("\n\nYup here i m in Getmindistance\n\n");
		
	
	
	double minAngle = -(-(Avelocity*Avelocity))/(2*Aacceleration);
	//printf("\nMinimum distance required:%.6g\t%.6g\n",mindist,coeff_friction);
	return	minAngle;

}
*/
/*
static double getTurnVelocitySquare(cJSON *turn){

	return (getRadius(turn)*coeff_friction);
}
*/
static double getTurnVelocity(cJSON *turn){

	return sqrt(getRadius(turn)*coeff_friction);
}

static double getThrottle(cJSON *piece){
//	double throttle = sqrt(getRadius(piece)*coeff_friction)/Vmax;
	//printf("\nTHROTTLE FROM FUNCTION:%.6g\t%.6g\t%.6g\n",throttle,getRadius(piece),coeff_friction);
	if(turboFired == 1 || current_carAngle > 50)
	return 0;		
	else return sqrt(getRadius(piece)*coeff_friction)/Vmax;		

}

static double mod(double x){

if(x<0)
return -x;
else return x;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}
/*
static cJSON *create_race(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *botId = cJSON_CreateObject();
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
    cJSON_AddItemToObject(data,"botId",botId);
        cJSON_AddStringToObject(data, "trackName", "france");
        cJSON_AddStringToObject(data, "password", "658");
        cJSON_AddNumberToObject(data, "carCount", 1);
	
	//printf("\nvishal :%s",cJSON_Print(make_msg("createRace", data)));
    return make_msg("createRace", data);
}
*/
static cJSON *turbo_msg()
{

 //cJSON *data = cJSON_CreateObject();
// cJSON_AddStringToObject(data, "data", "Yoo go turbo");
// cJSON_AddItemToObject(data,"data",botId);
 return make_msg("turbo", cJSON_CreateString("Yoo turbo"));
}

/*
static cJSON *join_race(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *botId = cJSON_CreateObject();
    cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
    cJSON_AddItemToObject(data,"data",botId);
    cJSON_AddStringToObject(data, "trackName", "france");
        cJSON_AddStringToObject(data, "password", "658");
        cJSON_AddNumberToObject(data, "carCount", 1);
	
    return make_msg("joinRace", data);
}
*/
static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *switch_msg(char *direction)
{
    return make_msg("switchLane", cJSON_CreateString(direction));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}

